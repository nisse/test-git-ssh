#! /bin/bash

set -eu

die() {
    echo "$@"
    exit 1
}

[[ $# = 3 ]] || die "Missing arguments. Usage: $0 ALLOWED-SIGNERS SIGNATURE-FILE ARTIFACT-FILE"

ALLOWED_SIGNERS="$1"
SIGNATURE="$2"
ARTIFACT="$3"

PRINCIPALS="$(ssh-keygen -Y find-principals -f "${ALLOWED_SIGNERS}" -s "${SIGNATURE}")"
PRINCIPAL="$(echo "${PRINCIPALS}" | head -1)"

exec ssh-keygen -Y verify -f "${ALLOWED_SIGNERS}" -s "${SIGNATURE}" -I "${PRINCIPAL}" -n "file" < "${ARTIFACT}"
