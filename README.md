Repository to try out ssh signatures on tags and/or commits.

Suggested local configuration:

  git config --local gpg.format ssh
  git config --local user.signingkey "key::$(< key.pub)"
  git config --local gpg.ssh.allowedSignersFile allowed_signers
  git config --local log.showSignature true

Signed commits can be made using git commit -S, and they are displayed
by git log. (Short option -s is somethign different, to add a signoff line).

Signed tags can be made using git tag -s. Signed tags are always
annotated, and should have a message.

Signing files is can be done using

    ssh-keygen -Y sign -f key.pub -n "file" MSG

(since only public key is given, ssh-agent is required). This can be
verified without any metadata using

    ssh-keygen -Y check-novalidate -f key.pub -n "file" -s SIG_FILE < MSG

To verify against a listed key, use

    ssh-keygen -Y verify -f allowed_signers -I id -n namespace -s SIG_FILE < MSG
